package com.example.thesport.features.domain.usecases

import com.example.thesport.features.domain.entities.TeamDomain
import com.example.thesport.features.domain.repository.ITeamDomainRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


class GetTeamUseCase@Inject constructor(private val iTeamDomainRepository: ITeamDomainRepository) :
    IGetTeamUseCase {

    override suspend fun invoke(param: String) = iTeamDomainRepository.getTeamById(param)
}

interface IGetTeamUseCase {

   suspend fun invoke(param: String): Flow<List<TeamDomain>>
}