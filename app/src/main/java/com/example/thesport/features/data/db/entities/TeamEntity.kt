package com.example.thesport.features.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "team")
data class TeamEntity(
    @PrimaryKey
    val idTeam: String = "",
    val strTeam: String = "",
    val intFormedYear: String = "",
    val strStadium: String = "",
    val strWebsite: String = "",
    val strFacebook: String = "",
    val strTwitter: String = "",
    val strInstagram: String = "",
    val strDescriptionEN: String = "",
    val strTeamBadge: String = "",
    val strTeamJersey: String = "",
    val strYoutube: String = ""
)