package com.example.thesport.features.data.di

import android.content.Context
import androidx.room.Room
import com.example.thesport.features.data.db.AppDatabase
import com.example.thesport.features.data.db.DAOs.EventDao
import com.example.thesport.features.data.db.DAOs.LeagueDao
import com.example.thesport.features.data.db.DAOs.TeamDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "thesport.db"
        ).fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun provideTeamDao(database: AppDatabase): TeamDao {
        return database.teamDao()
    }

    @Provides
    fun provideLeagueDao(database: AppDatabase): LeagueDao {
        return database.leagueDao()
    }

    @Provides
    fun provideEventDao(database: AppDatabase): EventDao {
        return database.eventDao()
    }

}