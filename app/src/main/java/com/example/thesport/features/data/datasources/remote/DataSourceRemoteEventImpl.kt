package com.example.thesport.features.data.datasources.remote

import com.example.thesport.core.converter.IConverter
import com.example.thesport.features.data.network.TheSportApiClient
import com.example.thesport.features.domain.entities.EventDomain
import javax.inject.Inject


class DataSourceRemoteEventImpl @Inject constructor(
    private val iConverter:@JvmSuppressWildcards  IConverter,
    private val theSportApiClient: TheSportApiClient
) : IDataSourceRemoteEvent {


    override suspend fun getEventById(id: String): List<EventDomain> {
        val response = theSportApiClient.getEventsByTeamId(id)
        val eventsDto = response.results

        return eventsDto.map {
            iConverter.convertEventDtoToDomain(it)
        }
    }
}

