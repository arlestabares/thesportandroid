package com.example.thesport.features

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class TheSportApplication : Application() {
}