package com.example.thesport.features.data.datasources.local

import com.example.thesport.core.converter.IConverter
import com.example.thesport.core.data.datasource.IBaseDataSourceLocalRepository
import com.example.thesport.features.data.db.DAOs.EventDao
import com.example.thesport.features.data.db.entities.EventEntity
import com.example.thesport.features.domain.entities.EventDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class DataSourceLocalEventImpl @Inject constructor(
    private val eventDao: EventDao,
    private val iConverter: IConverter
) : IBaseDataSourceLocalRepository<EventDomain> {


    override suspend fun save(data: EventDomain) {
        val eventEntity: EventEntity = iConverter.convertEventDomainToDbEntity(data)
        eventDao.insert(eventEntity)
    }

    override suspend fun saveAll(data: List<EventDomain>) {
        eventDao.insertAll(data.map {
            iConverter.convertEventDomainToDbEntity(it)
        })
    }

    override fun getById(id: String): Flow<List<EventDomain>> {
        return eventDao.getById(id).map {
            it.map {
                iConverter.convertDbEventEntityToDomain(it)
            }
        }
    }

    override fun getAll(): Flow<List<EventDomain>> {
        return eventDao.getAll().map {
            it.map {
                iConverter.convertDbEventEntityToDomain(it)
            }
        }
    }


}