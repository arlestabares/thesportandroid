package com.example.thesport.core.converter


import com.example.thesport.features.data.DTOs.EventDto
import com.example.thesport.features.data.DTOs.LeagueDto
import com.example.thesport.features.data.DTOs.TeamDto
import com.example.thesport.features.data.db.entities.EventEntity
import com.example.thesport.features.data.db.entities.LeagueEntity
import com.example.thesport.features.data.db.entities.TeamEntity
import com.example.thesport.features.domain.entities.EventDomain
import com.example.thesport.features.domain.entities.LeagueDomain
import com.example.thesport.features.domain.entities.TeamDomain
import javax.inject.Inject

class Converter @Inject constructor() : IConverter {

    private fun validateToString(value: String?) = value ?: ""

    override fun convertEventDtoToDomain(eventDto: EventDto): EventDomain {
        return EventDomain(eventDto.idEvent, eventDto.idHomeTeam, eventDto.strEvent)
    }

    override fun convertEventDomainToDbEntity(eventDomain: EventDomain): EventEntity {
        return EventEntity(eventDomain.idEvent, eventDomain.idTeam, eventDomain.strEvent)
    }

    override fun convertDbEventEntityToDomain(eventEntity: EventEntity): EventDomain {
        return EventDomain(eventEntity.id, eventEntity.idTeam, eventEntity.strEvent)
    }

    override fun convertLeagueDtoToDomain(leagueDto: LeagueDto): LeagueDomain {
        return LeagueDomain(leagueDto.idLeague, leagueDto.strLeague)
    }

    override fun convertLeagueDomainToDbEntity(leagueDomain: LeagueDomain): LeagueEntity {
        return LeagueEntity(leagueDomain.idLeague, leagueDomain.strLeague)
    }

    override fun convertDbLeagueEntityToDomain(leagueEntity: LeagueEntity): LeagueDomain {
        return LeagueDomain(leagueEntity.id, leagueEntity.leagueName)
    }

    override fun convertTeamDtoToDomain(teamDto: TeamDto): TeamDomain {
        return TeamDomain(
            teamDto.idTeam,
            teamDto.strTeam
        ).apply {
            intFormedYear = validateToString(teamDto.intFormedYear)
            strStadium = validateToString(teamDto.strStadium)
            strWebsite = validateToString(teamDto.strWebsite)
            strFacebook = validateToString(teamDto.strFacebook)
            strTwitter = validateToString(teamDto.strTwitter)
            strInstagram = validateToString(teamDto.strInstagram)
            strDescriptionEN = validateToString(teamDto.strDescriptionEN)
            strTeamBadge = validateToString(teamDto.strTeamBadge)
            strTeamJersey = validateToString(teamDto.strTeamJersey)
            strYoutube = validateToString(teamDto.strYoutube)
        }
    }

    override fun convertTeamDomainToDbEntity(teamDomain: TeamDomain): TeamEntity {
        return TeamEntity(
            teamDomain.idTeam,
            teamDomain.strTeam,
            teamDomain.intFormedYear,
            teamDomain.strStadium,
            teamDomain.strWebsite,
            teamDomain.strFacebook,
            teamDomain.strTwitter,
            teamDomain.strInstagram,
            teamDomain.strDescriptionEN,
            teamDomain.strTeamBadge,
            teamDomain.strTeamJersey,
            teamDomain.strYoutube

        )
    }

    override fun convertDbTeamEntityToDomain(teamEntity: TeamEntity): TeamDomain {
        return TeamDomain(
            teamEntity.idTeam,
            teamEntity.strTeam
        ).apply {
            intFormedYear = teamEntity.intFormedYear
            strStadium = teamEntity.strStadium
            strWebsite = teamEntity.strWebsite
            strFacebook = teamEntity.strFacebook
            strTwitter = teamEntity.strTwitter
            strInstagram = teamEntity.strInstagram
            strDescriptionEN = teamEntity.strDescriptionEN
            strTeamBadge = teamEntity.strTeamBadge
            strTeamJersey = teamEntity.strTeamJersey
            strYoutube = teamEntity.strYoutube
        }
    }
}