package com.example.thesport.features.data.repository

import com.example.thesport.core.data.datasource.IBaseDataSourceLocalRepository
import com.example.thesport.features.data.datasources.remote.IDataSourceRemoteTeam
import com.example.thesport.features.domain.entities.TeamDomain
import com.example.thesport.features.domain.repository.ITeamDomainRepository
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class TeamRepositoryImpl @Inject constructor(
    private val iDataSourceRemoteTeam: IDataSourceRemoteTeam,
    private val iBaseDataSourceLocalRepository: IBaseDataSourceLocalRepository<TeamDomain>
) :  ITeamDomainRepository {


    override suspend fun localSave(dataList: List<TeamDomain>) {
        dataList.forEach { teamDomain: TeamDomain ->
            iBaseDataSourceLocalRepository.save(teamDomain)
        }
    }

    override suspend fun getAll(leagueName: String) = flow {
        val response :List<TeamDomain> = iDataSourceRemoteTeam.getAllTeams(leagueName)
        localSave(response)
        emit(response)

    }

    override suspend fun getTeamById(paramId: String) = flow {
        val response: List<TeamDomain> = iDataSourceRemoteTeam.getTeamById(paramId)
        localSave(response)
        emit(response)
    }
}