package com.example.thesport.features.data.db.DAOs

import androidx.room.*
import com.example.thesport.features.data.db.entities.EventEntity
import kotlinx.coroutines.flow.Flow


@Dao
interface EventDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(event: EventEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(events: List<EventEntity>)

    @Query("SELECT * FROM event")
    fun getAll(): Flow<List<EventEntity>>

    @Query("SELECT * FROM event WHERE idTeam = :id")
    fun getById(id: String): Flow<List<EventEntity>>
}