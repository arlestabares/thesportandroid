package com.example.thesport.core.data.network

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import androidx.annotation.RequiresApi

class Network(private val context: Context) : IConnectivity {


    @RequiresApi(Build.VERSION_CODES.M)
    override fun hasNetwork(): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

}


interface IConnectivity {
    fun hasNetwork():Boolean
}