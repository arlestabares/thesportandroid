package com.example.thesport.features.domain.repository

import com.example.thesport.features.domain.entities.TeamDomain
import kotlinx.coroutines.flow.Flow

interface ITeamDomainRepository {

    suspend fun getAll(leagueName: String): Flow<List<TeamDomain>>
    suspend fun getTeamById(paramId: String): Flow<List<TeamDomain>>
    suspend fun localSave(dataList: List<TeamDomain>)
}

