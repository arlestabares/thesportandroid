package com.example.thesport.features.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "event")
data class EventEntity(
    @PrimaryKey
    var id: String,
    val idTeam: String="",
    val strEvent: String=""
)