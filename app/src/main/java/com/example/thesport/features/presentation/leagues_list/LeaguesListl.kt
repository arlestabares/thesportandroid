package com.example.thesport.features.presentation.home

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.thesport.features.presentation.viewmodels.LeaguesViewModel
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.rememberNavController
import com.example.thesport.features.domain.entities.LeagueDomain
import com.example.thesport.features.presentation.navigation.Destinations
import com.example.thesport.features.presentation.routes.Routes
import com.example.thesport.ui.theme.TheSportTheme


@Composable
fun LeaguesListScreen(
    naveController: NavController,
    viewModel: LeaguesViewModel = hiltViewModel()

) {
    val leaguesList by viewModel.getAllLeagues().observeAsState(initial = emptyList())
    LeaguesScreen(naveController, leaguesList)
}

@Composable
fun LeaguesScreen(
    naveController: NavController,
    leagues: List<LeagueDomain>

) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Row(
                        modifier = Modifier.padding(8.dp).fillMaxWidth(),
                        horizontalArrangement = Arrangement.aligned(
                            Alignment.CenterHorizontally
                        )
                    ) {
                        Text("Leagues")
                    }
                }
            )
        }
    ) {
        LazyColumn(
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            items(leagues) { league ->
                Box(
                    modifier = Modifier.padding(8.dp).fillMaxWidth().clickable {
                        naveController.navigate("${Destinations.leaguesTeamScreen.route}/${league.strLeague}")
                    },
                    contentAlignment = Alignment.Center

                ) {
                    Text(league.strLeague)
                }
            }
        }

    }
}

@Preview
@Composable
fun LeaguesScreenPreview() {

    TheSportTheme {

        LeaguesScreen(
            naveController = rememberNavController(),
            emptyList()

        )

    }
}