package com.example.thesport.features.data.repository

import com.example.thesport.core.data.datasource.IBaseDataSourceLocalRepository
import com.example.thesport.features.data.datasources.remote.IDataSourceRemoteLeague
import com.example.thesport.features.domain.entities.LeagueDomain
import com.example.thesport.features.domain.repository.ILeaguesDomainRepository
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class LeagueRepositoryImpl @Inject constructor(
    private val iBaseDataSourceLocalRepository: IBaseDataSourceLocalRepository<LeagueDomain>,
    private val iDataSourceRemoteLeague: IDataSourceRemoteLeague

) : ILeaguesDomainRepository {

    override suspend fun localSave(dataList: List<LeagueDomain>){
        return dataList.forEach { leagueDomain: LeagueDomain ->
            iBaseDataSourceLocalRepository.save(leagueDomain)
        }
    }

    override suspend fun getAllLeagues() = flow {
       val response= iDataSourceRemoteLeague.getAllLeagues()
        localSave(response)
        emit(response)
//        return response


    }
}