package com.example.thesport.features.data.response

import com.example.thesport.features.data.DTOs.EventDto
import javax.inject.Inject


data class EventResponse@Inject constructor(val results:List<EventDto>)