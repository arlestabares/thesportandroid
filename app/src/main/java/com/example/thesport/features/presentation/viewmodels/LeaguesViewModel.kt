package com.example.thesport.features.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thesport.features.domain.entities.LeagueDomain
import com.example.thesport.features.domain.usecases.IGetLeaguesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LeaguesViewModel @Inject constructor(
    private val iGetLeaguesUseCase: IGetLeaguesUseCase
) : ViewModel() {

    private var _leaguesMLiveData: MutableLiveData<List<LeagueDomain>> = MutableLiveData()
    var leagueLivedata: LiveData<List<LeagueDomain>> = _leaguesMLiveData

    fun getAllLeagues(): LiveData<List<LeagueDomain>> {
        viewModelScope.launch(Dispatchers.IO) {
            iGetLeaguesUseCase.invoke().collect {
                _leaguesMLiveData.postValue(it)
            }
        }
        return leagueLivedata
    }
}

