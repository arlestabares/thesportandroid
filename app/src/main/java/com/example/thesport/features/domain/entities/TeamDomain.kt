package com.example.thesport.features.domain.entities

import javax.inject.Inject

class TeamDomain @Inject constructor(
    val idTeam: String,
    val strTeam: String
) {
    lateinit var intFormedYear: String
    lateinit var strStadium: String
    lateinit var strWebsite: String
    lateinit var strFacebook: String
    lateinit var strTwitter: String
    lateinit var strInstagram: String
    lateinit var strDescriptionEN: String
    lateinit var strTeamBadge: String
    lateinit var strTeamJersey: String
    lateinit var strYoutube: String
}