package com.example.thesport.features.data.datasources.local

import com.example.thesport.core.converter.IConverter
import com.example.thesport.core.data.datasource.IBaseDataSourceLocalRepository
import com.example.thesport.features.data.db.DAOs.TeamDao
import com.example.thesport.features.data.db.entities.TeamEntity
import com.example.thesport.features.domain.entities.TeamDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class DataSourceLocalTeamImpl @Inject constructor(
    private val teamDao: TeamDao,
    private val iConverter: IConverter
) : IBaseDataSourceLocalRepository<TeamDomain> {


    override suspend fun save(data: TeamDomain) {
        val teamEntity: TeamEntity = iConverter.convertTeamDomainToDbEntity(data)
        teamDao.insert(teamEntity)
    }

    override suspend fun saveAll(data: List<TeamDomain>) {
        teamDao.insertAll(data.map {
            iConverter.convertTeamDomainToDbEntity(it)
        })
    }

    override fun getAll(): Flow<List<TeamDomain>> {
        return teamDao.getAll().map {
            it.map {
                iConverter.convertDbTeamEntityToDomain(it)

            }
        }
    }

    override fun getById(id: String): Flow<List<TeamDomain>> {
        return teamDao.getById(id).map {
            it.map {
                iConverter.convertDbTeamEntityToDomain(it)
            }
        }
    }
}