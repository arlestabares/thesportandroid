package com.example.thesport.features.domain.repository

import com.example.thesport.features.domain.entities.LeagueDomain
import kotlinx.coroutines.flow.Flow

interface ILeaguesDomainRepository {

    suspend fun getAllLeagues(): Flow<List<LeagueDomain>>
    suspend fun localSave(dataList: List<LeagueDomain>)
}