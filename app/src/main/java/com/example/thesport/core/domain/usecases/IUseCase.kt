package com.example.thesport.core.domain.usecases

import kotlinx.coroutines.flow.Flow

interface IUseCase<T> {

   suspend fun invoke(param: String): Flow<List<T>>
}