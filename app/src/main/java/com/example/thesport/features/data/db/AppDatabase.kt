package com.example.thesport.features.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.thesport.features.data.db.DAOs.EventDao
import com.example.thesport.features.data.db.DAOs.LeagueDao
import com.example.thesport.features.data.db.DAOs.TeamDao
import com.example.thesport.features.data.db.entities.EventEntity
import com.example.thesport.features.data.db.entities.LeagueEntity
import com.example.thesport.features.data.db.entities.TeamEntity

@Database(
    entities = [EventEntity::class, LeagueEntity::class, TeamEntity::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun eventDao(): EventDao
    abstract fun leagueDao(): LeagueDao
    abstract fun teamDao(): TeamDao

}