package com.example.thesport.features.data.di

import com.example.thesport.core.converter.Converter
import com.example.thesport.core.converter.IConverter
import com.example.thesport.features.data.datasources.remote.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RemoteNavigationModule {

    @Binds
    abstract fun bindDataSourceRemoteEvent(impl: DataSourceRemoteEventImpl): IDataSourceRemoteEvent

    @Binds
    abstract fun binDataSourceRemoteLeague(impl: DataSourceRemoteLeagueImpl): IDataSourceRemoteLeague

    @Binds
    abstract fun bindDataSourceRemoteTeam(impl: DataSourceRemoteTeamImpl): IDataSourceRemoteTeam

    @Binds
    abstract  fun bindConverter(impl: Converter):IConverter
}