package com.example.thesport.features.data.db.DAOs

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.thesport.features.data.db.entities.LeagueEntity
import kotlinx.coroutines.flow.Flow


@Dao
interface LeagueDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertList(leagues: List<LeagueEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(leagueEntity: LeagueEntity)

    @Query("SELECT * FROM league")
    fun getAll(): Flow<List<LeagueEntity>>
}