package com.example.thesport.features.data.datasources.remote

import com.example.thesport.core.converter.IConverter
import com.example.thesport.features.data.network.TheSportApiClient
import com.example.thesport.features.domain.entities.EventDomain
import com.example.thesport.features.domain.entities.TeamDomain
import javax.inject.Inject


class DataSourceRemoteTeamImpl @Inject constructor(
    private val iTheSportApiClient: TheSportApiClient,
    private val iConverter: IConverter
) : IDataSourceRemoteTeam {

    override suspend fun getTeamById(idTeam: String): List<TeamDomain> {
        val response = iTheSportApiClient.getTeam(idTeam)
        val teamDto = response.teams ?: throw  Exception("No team for this Category")

        return teamDto.map {
            iConverter.convertTeamDtoToDomain(it)
        }

//        return iTheSportApiClient.getTeam(idTeam).teamsList.map {
//            iConverter.convertTeamDtoToDomain(it)
//        }
    }

    override suspend fun getAllTeams(leagueName: String): List<TeamDomain> {
        val response = iTheSportApiClient.getAllTeams(leagueName)
        val teamsDto = response.teams ?: throw  Exception("No teams.... for this Category")
        return teamsDto.map {
            iConverter.convertTeamDtoToDomain(it)
        }
//        return iTheSportApiClient.getAllTeams(leagueName).teamsList.map {
//            iConverter.convertTeamDtoToDomain(it)
//        }
    }

    override suspend fun getEventsTeamById(idEvent: String): List<EventDomain> {
        return iTheSportApiClient.getEventsByTeamId(idEvent).results.map {
            iConverter.convertEventDtoToDomain(it)
        }
    }
}