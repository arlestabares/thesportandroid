package com.example.thesport.features.data.response

import com.example.thesport.features.data.DTOs.TeamDto
import javax.inject.Inject

data class TeamResponse @Inject constructor(val teams: List<TeamDto>?)