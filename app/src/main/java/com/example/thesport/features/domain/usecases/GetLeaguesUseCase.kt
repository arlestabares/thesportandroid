package com.example.thesport.features.domain.usecases
import com.example.thesport.features.domain.entities.LeagueDomain
import com.example.thesport.features.domain.repository.ILeaguesDomainRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


class GetLeaguesUseCase @Inject constructor(private val iLeaguesDomainRepository: ILeaguesDomainRepository) :
    IGetLeaguesUseCase {

    override suspend fun invoke() = iLeaguesDomainRepository.getAllLeagues()

}

interface IGetLeaguesUseCase {

   suspend  fun invoke(): Flow<List<LeagueDomain>>
}
