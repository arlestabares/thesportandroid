package com.example.thesport.features.data.db.entities

import androidx.room.Embedded
import androidx.room.Relation

data class TeamEvents(
    @Embedded val teamEntity: TeamEntity,
    @Relation(
        parentColumn = "idTeam",
        entityColumn = "idTeam"
    ) val events: List<EventEntity>
)