package com.example.thesport.features.presentation.home

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Facebook
import androidx.compose.material.icons.filled.Web
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import com.example.thesport.R
import com.example.thesport.features.domain.entities.EventDomain
import com.example.thesport.features.domain.entities.TeamDomain
import com.example.thesport.features.presentation.viewmodels.TeamDetailsViewModel
import com.example.thesport.ui.theme.TheSportTheme


@Composable
fun TeamDetailsScreen(
    idTeam: String,
    navController: NavController,
    viewModel: TeamDetailsViewModel = hiltViewModel()
) {
    val team by viewModel.getTeam(idTeam).observeAsState(initial = null)
    val events by viewModel.getEventsByTeamId(idTeam).observeAsState()
    team?.let { events?.let { it1 -> TeamScreen(navController, it, it1) } }

}

@OptIn(coil.annotation.ExperimentalCoilApi::class)
@ExperimentalCoilApi
@Composable
fun TeamScreen(
    navController: NavController,
    team: TeamDomain?,
    eventList: List<EventDomain>
) {
    Scaffold(
        topBar = {
            TopAppBar(title = {
                Text(
                    team!!.strTeam,
                )
            },
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = "Back",
                        )
                    }
                }
            )
        }
    ) {
        team?.let {

            Card(
                shape = RoundedCornerShape(8.dp),
                modifier = Modifier.padding(8.dp).fillMaxWidth()
                    .verticalScroll(rememberScrollState()),
            ) {
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    CardTeamBagde(team)
                    TeamDescription(team)
                    TeamJersey(team)
                    TeamEventsCardList(eventList, modifier = Modifier)
                    SocialMedia(modifier = Modifier, team)

                }
            }


        } ?: run {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                CircularProgressIndicator()
            }

        }
    }
}


@Composable
fun CardTeamBagde(team: TeamDomain) {
    Image(
        modifier = Modifier.fillMaxWidth().aspectRatio(16f / 9f),
        painter = rememberImagePainter(
            data = team.strTeamBadge,
            builder = {
                placeholder(R.drawable.placeholder)
                error(R.drawable.placeholder)
            }
        ),
        contentDescription = null,
        contentScale = ContentScale.FillWidth
    )
}

@Composable
fun TeamDescription(team: TeamDomain) {
    Column(
        modifier = Modifier.padding(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text("Team Fundation")
        Text(team.intFormedYear, modifier = Modifier.padding(vertical = 8.dp))
        Text("Description:", modifier = Modifier.padding(vertical = 8.dp))
        Text(team.strDescriptionEN)


    }
}

@Composable
fun TeamJersey(team: TeamDomain) {
    Column(
        modifier = Modifier.padding(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text("Jersey")
        Image(
            modifier = Modifier.size(width = 140.dp, height = 170.dp),
            painter = rememberImagePainter(
                data = team.strTeamJersey,
                builder = {
                    placeholder(R.drawable.placeholder)
                    error(R.drawable.placeholder)
                }
            ),
            contentDescription = null,
            contentScale = ContentScale.FillWidth
        )
    }
}

@Composable
fun TeamEventsCardList(eventList: List<EventDomain>, modifier: Modifier) {
    Column {
        Text("Events", modifier.padding(horizontal = 8.dp, vertical = 16.dp))
        LazyRow(
            contentPadding = PaddingValues(4.dp),
            horizontalArrangement = Arrangement.spacedBy(4.dp),
            modifier = Modifier.fillMaxSize(1F)
        ) {
            items(eventList) { event ->
                TeamEventsCard(modifier = modifier, event.strEvent)
            }
        }
    }
}

@Composable
fun TeamEventsCard(modifier: Modifier, text: String) {

    Card(
        modifier = modifier,
        border = BorderStroke(color = Color.Black, width = Dp.Hairline),
        shape = RoundedCornerShape(8.dp),
        backgroundColor = Color.Red
    ) {
        Row(
            modifier = Modifier.padding(start = 8.dp, top = 4.dp, end = 8.dp, bottom = 4.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(text = text)
        }
    }
}

@Composable
fun SocialMedia(modifier: Modifier, team: TeamDomain?) {
    Column {
        val context = LocalContext.current
        team?.let {

            Row(
                modifier = Modifier.padding(horizontal = 8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(
                    modifier = Modifier.padding(8.dp),
                    onClick = {
                        val intent =
                            Intent(Intent.ACTION_VIEW, Uri.parse("https://${team.strFacebook}"))
                        context.startActivity(intent)
                    },
                ) {
                    Icon(Icons.Filled.Facebook, contentDescription = "Button Social Media")
                }

                IconButton(
                    modifier = Modifier.padding(8.dp),
                    onClick = {
                        val intent =
                            Intent(Intent.ACTION_VIEW, Uri.parse("https://${team.strInstagram}"))
                        context.startActivity(intent)
                    },
                ) {
                    Icon(Icons.Filled.Facebook, contentDescription = "Button Social Media")
                }
                IconButton(
                    modifier = Modifier.padding(8.dp),
                    onClick = {
                        val intent =
                            Intent(Intent.ACTION_VIEW, Uri.parse("https://${team.strTwitter}"))
                        context.startActivity(intent)
                    },
                ) {
                    Icon(Icons.Filled.Facebook, contentDescription = "Button Social Media")
                }
                IconButton(
                    modifier = Modifier.padding(8.dp),
                    onClick = {
                        val intent =
                            Intent(Intent.ACTION_VIEW, Uri.parse("https://${team.strWebsite}"))
                        context.startActivity(intent)
                    },
                ) {
                    Icon(Icons.Filled.Web, contentDescription = "Button Social Media")
                }
            }
        } ?: run {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                CircularProgressIndicator()
            }

        }
    }
}


@Preview(name = "TeamPreview", showSystemUi = true)
@Composable
fun ChipPreview() {
    TeamEventsCard(modifier = Modifier, "Kamarena")
}


@Preview()
@Composable
fun TeamScreenPreview() {
    TheSportTheme {
//        TeamScreen(
//
//            navController = rememberNavController(),
//            team = TeamDomain(idTeam = "123", strTeam = "Alaves"),
//            eventList= {EventDomain(idTeam = "123",strEvent = "321",idEvent = "132")
//
//            }
//        )
    }

}