package com.example.thesport.features.data.db.DAOs

import androidx.room.*
import com.example.thesport.features.data.db.entities.TeamEntity
import com.example.thesport.features.data.db.entities.TeamEvents
import kotlinx.coroutines.flow.Flow

@Dao
interface TeamDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(team: TeamEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(teams: List<TeamEntity>)

    @Query("SELECT * FROM team")
    fun getAll(): Flow<List<TeamEntity>>

    @Query("SELECT * FROM team WHERE idTeam = :id")
    fun getById(id: String): Flow<List<TeamEntity>>

    @Transaction
    @Query("SELECT * FROM team")
    suspend fun getTeamEvents(): List<TeamEvents>
}