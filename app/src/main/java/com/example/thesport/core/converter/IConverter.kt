package com.example.thesport.core.converter

import com.example.thesport.features.data.DTOs.EventDto
import com.example.thesport.features.data.DTOs.LeagueDto
import com.example.thesport.features.data.DTOs.TeamDto
import com.example.thesport.features.data.db.entities.EventEntity
import com.example.thesport.features.data.db.entities.LeagueEntity
import com.example.thesport.features.data.db.entities.TeamEntity
import com.example.thesport.features.domain.entities.EventDomain
import com.example.thesport.features.domain.entities.LeagueDomain
import com.example.thesport.features.domain.entities.TeamDomain


interface IConverter {
    fun convertEventDtoToDomain(eventDto: EventDto): EventDomain
    fun convertEventDomainToDbEntity(eventDomain: EventDomain): EventEntity
    fun convertDbEventEntityToDomain(eventEntity: EventEntity): EventDomain

    fun convertLeagueDtoToDomain(leagueDto: LeagueDto): LeagueDomain
    fun convertLeagueDomainToDbEntity(leagueDomain: LeagueDomain): LeagueEntity
    fun convertDbLeagueEntityToDomain(leagueEntity: LeagueEntity): LeagueDomain

    fun convertTeamDtoToDomain(teamDto: TeamDto): TeamDomain
    fun convertTeamDomainToDbEntity(teamDomain: TeamDomain): TeamEntity
    fun convertDbTeamEntityToDomain(teamEntity: TeamEntity): TeamDomain
}