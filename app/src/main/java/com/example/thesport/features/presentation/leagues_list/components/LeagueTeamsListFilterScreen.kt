package com.example.thesport.features.presentation.home

import android.view.Gravity
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.compose.rememberImagePainter
import com.example.thesport.R
import com.example.thesport.features.domain.entities.TeamDomain
import com.example.thesport.features.presentation.routes.Routes
import com.example.thesport.features.presentation.viewmodels.TeamsListViewModel
import com.example.thesport.ui.theme.TheSportTheme


//const val strLeague = "Spanish La Liga"

@Composable
fun LeagueTeamsFilterScreen(
    strLeague: String,
    navController: NavController,
    viewModel: TeamsListViewModel = hiltViewModel()
) {

    val teamList by viewModel.getTeams(leagueName = strLeague).observeAsState(initial = emptyList())
    FilterTeamCard(strLeague, navController, teamList)
}

@Composable
fun FilterTeamCard(
    strLeague: String,
    navController: NavController,
    teamList: List<TeamDomain>
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.aligned(Alignment.CenterHorizontally)
                    ) {
                        Text(strLeague)
                    }
                },
                navigationIcon = {
                    IconButton(onClick = {
                        navController.popBackStack()
                    }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = "Buttom back "
                        )
                    }
                }
            )
        }

    ) {
        LazyColumn {
            items(teamList) { team ->
                Card(
                    shape = RoundedCornerShape(8.dp),
                    modifier = Modifier.padding(8.dp).fillMaxWidth().clickable {
                        navController.navigate("${Routes.TEAM_DETAILS}/${team.idTeam}")
                    }
                ) {
                    Column(
                        modifier = Modifier.padding(16.dp).fillMaxWidth(),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Box(
                            modifier = Modifier.padding(8.dp),
//                            contentAlignment = Alignment.Center
                        ) {
                            Text(team.strTeam, textAlign = TextAlign.Center)
                        }
                        Image(
                            modifier = Modifier.fillMaxWidth().aspectRatio(16f / 9f),
                            painter = rememberImagePainter(
                                data = team.strTeamBadge,
                                builder = {
                                    placeholder(R.drawable.placeholder)
                                    error(R.drawable.placeholder)
                                }
                            ),
                            contentDescription = null,
                            contentScale = ContentScale.FillWidth
                        )
                        Column(
                            modifier = Modifier.padding(8.dp),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Text("Estadio")
                            Text(
                                team.strStadium,
                                fontSize = 18.sp,
                                fontWeight = FontWeight.Bold
                            )
                            //                           Text(team.strDescriptionEN, maxLines = 3)
                        }
                    }
                }
            }
        }
    }
}


@Composable
@Preview
fun FilterTeamsScreenPreview() {
    TheSportTheme {
        TeamCard(
            navController = rememberNavController(),
            teamList = arrayListOf(
                TeamDomain(idTeam = "1", strTeam = "Alaves")
            )
        )
    }
}
