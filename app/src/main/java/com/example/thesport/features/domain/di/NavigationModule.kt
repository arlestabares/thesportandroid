package com.example.thesport.features.domain.di

import com.example.thesport.core.domain.usecases.IUseCase
import com.example.thesport.features.domain.entities.EventDomain
import com.example.thesport.features.domain.entities.TeamDomain
import com.example.thesport.features.domain.usecases.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class NavigationModule {

    @Binds
    abstract fun bindGetLeagueUseCase(impl: GetLeaguesUseCase): IGetLeaguesUseCase

    @Binds
    abstract fun bindGetAllTeamsFromLeagueUseCase(impl: GetTeamsUseCase): IUseCase<TeamDomain>

    @Binds
    abstract fun bindGetEventUseCase(impl: GetEventsUseCase): IUseCase<EventDomain>

    @Binds
    abstract fun bindGetTeamUseCase(impl: GetTeamUseCase): IGetTeamUseCase
}


