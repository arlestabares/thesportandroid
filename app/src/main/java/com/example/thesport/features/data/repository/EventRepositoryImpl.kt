package com.example.thesport.features.data.repository

import com.example.thesport.core.data.datasource.IBaseDataSourceLocalRepository
import com.example.thesport.features.data.datasources.remote.IDataSourceRemoteEvent
import com.example.thesport.features.domain.entities.EventDomain
import com.example.thesport.features.domain.repository.IEventsDomainRepository
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class EventRepositoryImpl @Inject constructor(
    private val iBaseDataSourceLocalRepository: IBaseDataSourceLocalRepository<EventDomain>,
    private val iDataSourceRemoteEvent: IDataSourceRemoteEvent
) : IEventsDomainRepository {


    override suspend fun localSave(dataList: List<EventDomain>) {
        return dataList.forEach { eventDomain: EventDomain ->
            iBaseDataSourceLocalRepository.save(eventDomain)
        }
    }

    override suspend fun getEventsById(eventId: String) = flow {
        val response: List<EventDomain> = iDataSourceRemoteEvent.getEventById(eventId)
        localSave(response)
        emit(response)
    }




}