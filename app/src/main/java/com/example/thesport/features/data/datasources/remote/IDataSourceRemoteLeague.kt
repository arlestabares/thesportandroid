package com.example.thesport.features.data.datasources.remote

import com.example.thesport.features.domain.entities.LeagueDomain
import kotlinx.coroutines.flow.Flow

interface IDataSourceRemoteLeague {

    suspend fun getAllLeagues(): List<LeagueDomain>
}