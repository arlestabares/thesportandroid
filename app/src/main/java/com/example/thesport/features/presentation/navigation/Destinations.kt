package com.example.thesport.features.presentation.navigation

sealed class Destinations(
    val route: String
) {
    object spanishLeagueScreen: Destinations("SPANISH_LEAGUE")
    object spanishLeagueDetailsScreen: Destinations("TEAM_DETAILS")
    object leaguesListScreen:Destinations("LEAGUES_LIST")
    object leaguesTeamScreen:Destinations("LEAGUES_TEAMS")
}