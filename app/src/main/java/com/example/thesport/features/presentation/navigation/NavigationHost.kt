package com.example.thesport.features.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.thesport.features.presentation.home.LeagueTeamsFilterScreen
import com.example.thesport.features.presentation.home.LeagueTeamsListScreen
import com.example.thesport.features.presentation.home.LeaguesListScreen
import com.example.thesport.features.presentation.home.TeamDetailsScreen


@Composable
fun NavigationHost() {
    val navController = rememberNavController()

    NavHost(navController, startDestination = Destinations.spanishLeagueScreen.route) {

        composable(Destinations.spanishLeagueScreen.route) {
            LeagueTeamsListScreen("Spanish La Liga", navController)
        }
        composable("${Destinations.spanishLeagueDetailsScreen.route}/{idTeam}") {
            it.arguments?.getString("idTeam")?.let { idTeam ->
                TeamDetailsScreen(idTeam, navController)
            }
        }
        composable(Destinations.leaguesListScreen.route) {
            LeaguesListScreen(navController)
        }
        composable("${Destinations.leaguesTeamScreen.route}/{strLeague}") {
            it.arguments?.getString("strLeague")?.let { strLeague ->
                LeagueTeamsFilterScreen(strLeague, navController)
            }
        }
    }

}
