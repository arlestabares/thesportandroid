package com.example.thesport.features.data.di

import com.example.thesport.core.converter.IConverter
import com.example.thesport.core.data.datasource.IBaseDataSourceLocalRepository
import com.example.thesport.features.data.datasources.local.DataSourceLocalEventImpl
import com.example.thesport.features.data.datasources.local.DataSourceLocalLeagueImpl
import com.example.thesport.features.data.datasources.local.DataSourceLocalTeamImpl
import com.example.thesport.features.domain.entities.EventDomain
import com.example.thesport.features.domain.entities.LeagueDomain
import com.example.thesport.features.domain.entities.TeamDomain
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
abstract class LocalNavigationModule {

    @Binds
    abstract fun bindDataSourceLocalEvent(impl: DataSourceLocalEventImpl): IBaseDataSourceLocalRepository<EventDomain>

    @Binds
    abstract fun bindDataSourceLocalLeague(impl: DataSourceLocalLeagueImpl): IBaseDataSourceLocalRepository<LeagueDomain>

    @Binds
    abstract fun bindDataSourceLocalTeam(impl: DataSourceLocalTeamImpl): IBaseDataSourceLocalRepository<TeamDomain>


}