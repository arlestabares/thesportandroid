package com.example.thesport.features.domain.usecases

import com.example.thesport.core.domain.usecases.IUseCase
import com.example.thesport.features.domain.entities.TeamDomain
import com.example.thesport.features.domain.repository.ITeamDomainRepository
import javax.inject.Inject


class GetTeamsUseCase @Inject constructor(private val iTeamDomainRepository: ITeamDomainRepository) :
    IUseCase<TeamDomain> {

    override suspend fun invoke(param: String) = iTeamDomainRepository.getAll(param)
}


