package com.example.thesport.features.data.datasources.remote

import com.example.thesport.features.domain.entities.EventDomain
import kotlinx.coroutines.flow.Flow

interface IDataSourceRemoteEvent {

    suspend fun getEventById(id:String):List<EventDomain>
}