package com.example.thesport.features.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thesport.features.domain.entities.EventDomain
import com.example.thesport.features.domain.entities.TeamDomain
import com.example.thesport.features.domain.usecases.GetEventsUseCase
import com.example.thesport.features.domain.usecases.IGetTeamUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TeamDetailsViewModel @Inject constructor(
    private val iGetEventsUseCase: GetEventsUseCase,
    private val iGetTeamUseCase: IGetTeamUseCase
) : ViewModel() {


    private var teamMLivedata: MutableLiveData<TeamDomain> = MutableLiveData()
    var teamLivedata: LiveData<TeamDomain> = teamMLivedata

    private var eventsMLivedata: MutableLiveData<List<EventDomain>> = MutableLiveData()
    var eventsLivedata: LiveData<List<EventDomain>> = eventsMLivedata

    fun getTeam(idTeam: String): LiveData<TeamDomain> {
        viewModelScope.launch(Dispatchers.IO) {
            iGetTeamUseCase.invoke(idTeam).collect {
                it.map {
                    teamMLivedata.postValue(it)
                }
            }
        }
        return teamLivedata
    }

    fun getEventsByTeamId(idTeam: String): LiveData<List<EventDomain>> {
        viewModelScope.launch(Dispatchers.IO) {
            iGetEventsUseCase.invoke(idTeam).collect {
                eventsMLivedata.postValue(it)
            }
        }
        return eventsLivedata
    }
}

