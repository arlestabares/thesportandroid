package com.example.thesport.core.data.datasource

import com.example.thesport.features.domain.entities.LeagueDomain
import kotlinx.coroutines.flow.Flow

interface IBaseDataSourceLocalRepository<T> {

    suspend fun save(data: T)
    suspend fun saveAll(data: List<T>)

    fun getAll(): Flow<List<T>>
    fun getById(id: String): Flow<List<T>>

}