package com.example.thesport.features.data.response

import com.example.thesport.features.data.DTOs.LeagueDto
import javax.inject.Inject

data class LeagueResponse @Inject constructor(val leagues: List<LeagueDto>)