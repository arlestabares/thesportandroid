package com.example.thesport.features.data.datasources.local

import com.example.thesport.core.converter.IConverter
import com.example.thesport.core.data.datasource.IBaseDataSourceLocalRepository
import com.example.thesport.features.data.db.DAOs.LeagueDao
import com.example.thesport.features.data.db.entities.LeagueEntity
import com.example.thesport.features.domain.entities.LeagueDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class DataSourceLocalLeagueImpl @Inject constructor(
    private val leagueDao: LeagueDao,
    private val iConverter: IConverter
) : IBaseDataSourceLocalRepository<LeagueDomain> {


    override suspend fun save(data: LeagueDomain) {
        val leagueEntity: LeagueEntity = iConverter.convertLeagueDomainToDbEntity(data)
        leagueDao.insert(leagueEntity)
    }

    override suspend fun saveAll(data: List<LeagueDomain>) {
        leagueDao.insertList(data.map {
            iConverter.convertLeagueDomainToDbEntity(it)
        })
    }

    override fun getAll(): Flow<List<LeagueDomain>> {
        return leagueDao.getAll().map {
            it.map {
                iConverter.convertDbLeagueEntityToDomain(it)
            }
        }
    }

    override fun getById(id: String): Flow<List<LeagueDomain>> {
        return leagueDao.getAll().map {
            it.map {
                iConverter.convertDbLeagueEntityToDomain(it)
            }
        }
    }
}