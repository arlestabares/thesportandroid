package com.example.thesport.features.domain.entities

import javax.inject.Inject

data class EventDomain @Inject constructor(
    val idEvent: String,
    val idTeam: String,
    val strEvent: String
)