package com.example.thesport.features.domain.repository

import com.example.thesport.features.domain.entities.EventDomain
import kotlinx.coroutines.flow.Flow

interface IEventsDomainRepository {

    suspend fun getEventsById(eventId: String): Flow<List<EventDomain>>
    suspend fun localSave(dataList:List<EventDomain>)
}