package com.example.thesport.features.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "league")
data class LeagueEntity(
    @PrimaryKey
    var id: String,
    var leagueName: String
)