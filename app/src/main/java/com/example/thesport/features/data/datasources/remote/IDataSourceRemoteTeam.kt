package com.example.thesport.features.data.datasources.remote

import com.example.thesport.features.domain.entities.EventDomain
import com.example.thesport.features.domain.entities.TeamDomain

interface IDataSourceRemoteTeam {

    suspend fun getTeamById(idTeam:String):List<TeamDomain>
    suspend fun getAllTeams(leagueName:String):List<TeamDomain>
    suspend fun getEventsTeamById(idEvent:String): List<EventDomain>
}