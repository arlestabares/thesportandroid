package com.example.thesport.features.presentation.routes

object Routes {

    const val LEAGUES_LIST = "LEAGUES_LIST"
    const val TEAM_DETAILS = "TEAM_DETAILS"
    const val SPANISH_LEAGUE = "SPANISH_LEAGUE"
    const val LEAGUE_TEAMS = "LEAGUE_TEAMS"
}