package com.example.thesport.features.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thesport.features.domain.entities.TeamDomain
import com.example.thesport.features.domain.usecases.GetTeamsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TeamsListViewModel @Inject constructor(
    private val iGetTeamsUseCase: GetTeamsUseCase
) : ViewModel() {

    private var _teamMLivedata: MutableLiveData<List<TeamDomain>> = MutableLiveData()
    var teamLivedata: LiveData<List<TeamDomain>> = _teamMLivedata


    fun getTeams(leagueName: String): LiveData<List<TeamDomain>> {
        viewModelScope.launch(Dispatchers.IO) {
            iGetTeamsUseCase.invoke(leagueName).collect {
                _teamMLivedata.postValue(it)
            }
        }
        return teamLivedata
    }
}

