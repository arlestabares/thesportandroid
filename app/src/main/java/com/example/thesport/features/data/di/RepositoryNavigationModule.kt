package com.example.thesport.features.data.di

import com.example.thesport.features.data.repository.EventRepositoryImpl
import com.example.thesport.features.data.repository.LeagueRepositoryImpl
import com.example.thesport.features.data.repository.TeamRepositoryImpl
import com.example.thesport.features.domain.repository.IEventsDomainRepository
import com.example.thesport.features.domain.repository.ILeaguesDomainRepository
import com.example.thesport.features.domain.repository.ITeamDomainRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryNavigationModule {

    @Binds
    abstract fun bindEventRepository(impl: EventRepositoryImpl): IEventsDomainRepository

    @Binds
    abstract fun binLeagueRepository(impl: LeagueRepositoryImpl): ILeaguesDomainRepository

    @Binds
    abstract fun binTeamRepository(impl: TeamRepositoryImpl): ITeamDomainRepository


}