package com.example.thesport.features.domain.usecases

import com.example.thesport.core.domain.usecases.IUseCase
import com.example.thesport.features.domain.entities.EventDomain
import com.example.thesport.features.domain.repository.IEventsDomainRepository
import javax.inject.Inject


class GetEventsUseCase @Inject constructor(private val iEventsDomainRepository: IEventsDomainRepository) :
    IUseCase<EventDomain> {

    override suspend fun invoke(eventId: String) = iEventsDomainRepository.getEventsById(eventId)

}
