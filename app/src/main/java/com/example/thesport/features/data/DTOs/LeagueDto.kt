package com.example.thesport.features.data.DTOs


data class LeagueDto(val idLeague: String, val strLeague: String)