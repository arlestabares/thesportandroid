package com.example.thesport.buildsrc

object Versions {
    const val ktLint = "0.41.0"
}

object Libs {
    const val ktLint = "com.pinterest:ktlint:${Versions.ktLint}"
    const val androidGradlePlugin = "com.android.tools.build:gradle:7.0.2"

    object Coil {
        const val compose = "io.coil-kt:coil-compose:1.3.0"
    }

    object GoogleMaps {
        const val maps = "com.google.android.libraries.maps:maps:3.1.0-beta"
        const val mapsKtx = "com.google.maps.android:maps-v3-ktx:3.1.0"
    }

    object Volley {
        const val volley = "com.android.volley:volley:1.2.0"
    }

    object Accompanist {
       private const val version = "0.16.0"
        const val insets = "com.google.accompanist:accompanist-insets:$version"
    }

    object Kotlin {
        private const val version = "1.5.21"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$version"
        const val gradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$version"
        const val extensions = "org.jetbrains.kotlin:kotlin-android-extensions:$version"

        object Coroutines {
            private const val version = "1.5.0"
            const val test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:$version"
            const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$version"
        }
    }

    object AndroidX {
        object Activity {
            private const val version = "1.3.1"
            const val activityCompose = "androidx.activity:activity-compose:$version"
        }

        object Core {
            const val androidxCoreKtx = "androidx.core:core-ktx:1.6.0"

        }

        const val appcompat = "androidx.appcompat:appcompat:1.3.0"
        const val material = "com.google.android.material:material:1.4.0"
        const val navigationCompose= "androidx.navigation:navigation-compose:2.4.0-alpha05"

        object Compose {
         private  const val version = "1.0.1"

            const val composeUi = "androidx.compose.ui:ui:$version"
            const val tooling = "androidx.compose.ui:ui-tooling:$version"
            const val runtime = "androidx.compose.runtime:runtime:$version"
            const val uiTestJunit = "androidx.compose.ui:ui-test-junit4:$version"
            const val material = "androidx.compose.material:material:$version"
            const val animation = "androidx.compose.animation:animation:$version"
            const val foundation = "androidx.compose.foundation:foundation:$version"
            const val layout = "androidx.compose.foundation:foundation-layout:$version"
            const val uiToolingPreview = "androidx.compose.ui:ui-tooling-preview:$version"
            const val composeLivedata= "androidx.compose.runtime:runtime-livedata:$version"
            const val  materialIconsExtended=  "androidx.compose.material:material-icons-extended:$version"
        }

        object Lifecycle {
            private const val version = "2.3.1"
            const val viewModelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:$version"
            const val runtimeKtx = "androidx.lifecycle:lifecycle-runtime-ktx:$version"
            const val viewModelCompose =
                "androidx.lifecycle:lifecycle-viewmodel-compose:1.0.0-alpha07"
        }

        object Test {
            private const val version = "1.3.0"
            const val runner = "androidx.test:runner:$version"
            const val rules = "androidx.test:rules:$version"

            object Ext {
                private const val version = "1.1.3"
                const val junit = "androidx.test.ext:junit:$version"
                const val junitKtx = "androidx.test.ext:junit-ktx:$version"
            }

            const val espressoCore = "androidx.test.espresso:espresso-core:3.4.0"
        }
    }

    object Room {
        private const val version = "2.3.0"

        const val ktx = "androidx.room:room-ktx:$version"
        const val runtime = "androidx.room:room-runtime:$version"
        const val roomRxJava2 = "androidx.room:room-rxjava2:$version"
        const val kaptCompiler = "androidx.room:room-compiler:$version"
        const val annotationProcessor = "androidx.room:room-compiler:$version"
    }


    object Hilt {
        private const val version = "2.38.1"

        const val android = "com.google.dagger:hilt-android:$version"
        const val compiler = "com.google.dagger:hilt-compiler:$version"
        const val kapt = "com.google.dagger:hilt-android-compiler:$version"
        const val testing = "com.google.dagger:hilt-android-testing:$version"
        const val gradlePlugin = "com.google.dagger:hilt-android-gradle-plugin:$version"
        const val navigationCompose = "androidx.hilt:hilt-navigation-compose:1.0.0-alpha03"


    }

    object JUnit {
        private const val version = "4.+"
        const val junit = "junit:junit:$version"
    }

    object Retrofit {
        private const val version = "2.8.1"

        const val retrofit = "com.squareup.retrofit2:retrofit:$version"
        const val gson = "com.squareup.retrofit2:converter-gson:$version"
    }

    object Okhttp {
        private const val version = "4.9.1"
        const val okhttp = "com.squareup.okhttp3:okhttp:$version"
        const val interceptor = "com.squareup.okhttp3:logging-interceptor:$version"
    }
}

