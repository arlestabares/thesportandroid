package com.example.thesport.features.data.datasources.remote

import com.example.thesport.core.converter.IConverter
import com.example.thesport.features.data.network.TheSportApiClient
import com.example.thesport.features.domain.entities.LeagueDomain
import javax.inject.Inject

class DataSourceRemoteLeagueImpl @Inject constructor(
    private val iTheSportApiClient: TheSportApiClient,
    private val iConverter:@JvmSuppressWildcards  IConverter
) : IDataSourceRemoteLeague {


    override suspend fun getAllLeagues(): List<LeagueDomain> {
        return iTheSportApiClient.getAllLeagues().leagues.map {
            iConverter.convertLeagueDtoToDomain(it)
        }
    }
}