package com.example.thesport.features.data.network

import com.example.thesport.features.data.response.EventResponse
import com.example.thesport.features.data.response.LeagueResponse
import com.example.thesport.features.data.response.TeamResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface TheSportApiClient {

    @GET("all_leagues.php")
    suspend fun getAllLeagues(): LeagueResponse

    @GET("lookupteam.php")
    suspend fun getTeam(@Query("id") idTeam: String): TeamResponse

    @GET("eventslast.php")
    suspend fun getEventsByTeamId(@Query("id") idTeam: String): EventResponse

    @GET("search_all_teams.php")
    suspend fun getAllTeams(@Query("l") leagueName: String): TeamResponse
}