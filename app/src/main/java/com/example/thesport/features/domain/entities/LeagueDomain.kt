package com.example.thesport.features.domain.entities

import javax.inject.Inject

data class LeagueDomain @Inject constructor(val idLeague: String, val strLeague: String)